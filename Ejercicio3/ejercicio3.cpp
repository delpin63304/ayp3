#include <iostream>

int main() {
    int cantidadNumerosComparar;

    printf("Ingrese cuantos números desea evaluar: ");
    scanf("%d", &cantidadNumerosComparar);

    if (cantidadNumerosComparar > 0){
        int numeros[cantidadNumerosComparar];
        int numero;

        for (int i = 0; i<cantidadNumerosComparar;i++){
            printf("Ingrese el siguiente número: ");
            scanf("%d", &numero);

            numeros[i] = numero;
        }

        int min = numeros[0];
        for (int i = 1; i < sizeof(numeros) / sizeof(int); i++) {
            if (numeros[i] < min) {
                min = numeros[i];
            }
        }
        printf("Mínimo: %d", min);

        return 0;
    }
}

#include <iostream>

int main() {
    int cantidadNumeros;

    printf("De cuantos números querés sacar el promedio: ");
    scanf("%d", &cantidadNumeros);

    if (cantidadNumeros > 0){
        int numero;
        int sum;

        for (int i = 0; i<cantidadNumeros;i++){
            printf("Ingrese el siguiente número: ");
            scanf("%d", &numero);

            sum += numero;
        }

        float avg = (float) sum / cantidadNumeros;
        printf("Promedio: %f", avg);

        return 0;
    }
}
